<!-- data for home screen -->
<?php

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
    <link rel="stylesheet" href="theme_home.css">
</head>

<body>
    <div class="wrapper">

        <div class="form-control">
            <form action="">

                <!-- selection department -->
                <div class="depart_label">
                    <label class="label-custom-1">Khoa</label>

                    <select class="custom" name="" id="">
                        <!-- option empty -->
                        <option value=""></option>
                        <?php
                        foreach (array_keys($department) as $dep) {
                            echo '
                                <option value="' . $dep . '">' . $department[$dep] . '</option>
                            ';
                        }
                        ?>
                    </select>
                </div>

                <!-- Key words -->
                <div class="keyword_label">
                    <label class="label-custom-1" >Từ khóa</label>
                    <input class="input-keyword" type="text">
                </div>

                <!-- search button -->
                <div class="btn-search">
                    <button class="btn-custom-1">Tìm kiếm</button>
                </div>

            </form>
        </div>

        <div class="row-content">
            <label for="">Số sinh viên tìm thấy: </label> <span>XXX</span>
        </div>
        <form action="form.php">
            <div class="btn-add">
                <button type="submit" class="btn-custom-2">Thêm</button>
            </div>
        </form>
        
        <table>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            <tr>
                <td>1</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>2</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>3</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
            <tr>
                <td>4</td>
                <td>Nguyễn Văn A</td>
                <td>Khoa học máy tính</td>
                <td>
                    <div class="btn-action">
                        <button>Xóa</button>
                        <button>Sửa</button>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</body>

</html>