<?php
    session_start();
    // $img = $_SESSION["img_encode"]
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="theme_submit.css">
    <title>Submit</title>
</head>
<body>

    <div class="wrapper">
        <form action="" method="POST" enctype="multipart/form-data">

            <div class="name_input">
                <label>Họ và tên</label>
                <?php
                    print_r($_SESSION['hoten'])
                ?>
            </div>

            <div class="sex_select">
                <label>Giới tính</label>
                <?php
                    print_r($_SESSION['sex_array'][$_SESSION['sex']])
                ?>
            </div>

            <div class="department">
                <label>Phân khoa</label>
                <?php
                    print_r($_SESSION['department_array'][$_SESSION['department']])
                ?>
            </div>

            <div class="date_input">
                <label>Ngày sinh</label>
                <?php
                    print_r($_SESSION['birthday'])
                ?>
            </div>

            <div class="address_input">
                <label>Địa chỉ</label>
                <?php
                    if ($_SESSION['address'] == null) {
                        echo "";
                    } else 
                        print_r($_SESSION['address']);
                ?>
            </div>

            <div class="image_input">
                <label>Hình ảnh</label>
                <?php
                    // echo '<img class="picture" src="' . $img . '" >';
                    $nameImage = $_SESSION['image'];
                    echo "<img class=\"picture\" src=\"upload/$nameImage\" alt=\"\" title=\"\" />";
                ?>
            </div>

            <button type="submit">Xác nhận</button>
        </form>
    </div>

</body>
</html>