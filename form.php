<?php
// Create folder upload
if (!file_exists('C:\xampp\htdocs\day05\upload')) {
    mkdir('C:\xampp\htdocs\day05\upload', 0777, true);
}

date_default_timezone_set("Asia/Ho_Chi_Minh");
$time = date("_YmdHis");

$sex = array(
    "Nam",
    "Nữ"
);

$department = array(
    "MAT" => "Khoa học máy tính",
    "KDL" => "Khoa học vật liệu"
);

?>

<?php
$error = "";
session_start();


function validateDate($date, $format = 'd/m/Y')
{
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) === $date;
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (!isset($_POST['hoten']) || $_POST['hoten'] == null) {
        $error .= "Hãy nhập tên.<br>";
    }

    if (!isset($_POST['sex']) || $_POST['sex'] == null) {
        $error .= "Hãy chọn giới tính.<br>";
    }

    if (!isset($_POST['department']) || $_POST['department'] == null) {
        $error .= "Hãy chọn phân khoa.<br>";
    }

    if (!isset($_POST['birthday']) || $_POST['birthday'] == null) {
        $error .= "Hãy nhập ngày sinh.<br>";
    } else if (!validateDate($_POST['birthday'])) {
        $error .= "Hãy nhập ngày sinh đúng định dạng.";
    }

    if ($_POST['hoten'] != null 
        && $_POST['sex'] != null 
        && $_POST['department'] != null 
        && $_POST['birthday'] != null 
        && validateDate($_POST['birthday'])) {
        header("location: submit_form.php");
        $target_dir = "upload/";
        $tail = explode(".", $_FILES["image"]["name"]); // tail of file
        $filenameFormat = pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME).$time.".".end($tail); // change file name
        $target_file = $target_dir . $filenameFormat;
        move_uploaded_file($_FILES['image']['tmp_name'], $target_file);
    }

    print_r($_FILES['image']);

    // print_r($_SESSION);
    $_SESSION = $_POST;
    $_SESSION['image'] = $filenameFormat;

}
$_SESSION['sex_array'] = $sex;
$_SESSION['department_array'] = $department;
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Image show -->

    <!-- Bootstrap -->
    <script type="text/javascript" src='https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.3.min.js'></script>
    <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.min.js'></script>
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css' media="screen" />
    <!-- Bootstrap -->
    <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->

    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="theme.css">
    <title>Sign In</title>
</head>

<body>

    <script type="text/javascript">
        $(function() {
            $('#txt_date').datepicker({
                format: "dd/mm/yyyy"
            });
        });
    </script>

    <script type="text/javascript">
        // function readURL(input) {
        //     console.log(input.files[0]);
        //     if (input.files && input.files[0]) {
        //         var reader = new FileReader();
        //         $('#blah').css("display", "block");
        //         reader.onload = function(e) {
        //             $('#blah')
        //                 .attr('src', e.target.result);

        //             $('.img_encode').val(e.target.result);
        //         };

        //         reader.readAsDataURL(input.files[0]);
        //     } else {
        //         $('#blah').css("display", "none");
        //     }
        // }

        function validateFileType(input) {
            var fileName = input.files[0].name;
            var idxDot = fileName.lastIndexOf(".") + 1;
            var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
            if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
                // readURL(input);
            } else {
                document.getElementById("chooseFile").value = "";
                alert("Chỉ hỗ trợ định dạng ảnh!");
            }
        }
    </script>

    <div class="wrapper">
        <form action="" method="POST" enctype="multipart/form-data">
            <div class="error">
                <?php
                echo $error
                ?>
            </div>

            <div class="name_input">
                <label class="red_star">Họ và tên</label>
                <input class="input_name" type="text" name="hoten">
            </div>

            <div class="sex_select">
                <label class="red_star">Giới tính</label>
                <?php
                for ($i = 0; $i < count($sex); $i++) {
                    echo "
                            <input class=\"input_sex\" name = \"sex\" type=\"radio\" value=\"$i\">{$sex[$i]}
                        ";
                }
                ?>
            </div>

            <div class="department">
                <label class="red_star">Phân khoa</label>

                <select class="custom" name="department">
                    <option value=""></option>
                    <?php
                    foreach (array_keys($department) as $dep) {
                        echo '
                                <option value="' . $dep . '">' . $department[$dep] . '</option>
                            ';
                    }
                    ?>
                </select>

            </div>

            <div class="date_input">
                <label class="red_star">Ngày sinh</label>
                <input id="txt_date" type="text" class="input_date" name="birthday" placeholder="dd/mm/yyyy" />
            </div>

            <div class="address_input">
                <label>Địa chỉ</label>
                <input class="input_name" type="text" name="address">
            </div>

            <div class="image_input">
                <label>Hình ảnh</label>
                <input class="choose_file" id="chooseFile" type="file" name="image" accept=".jpg,.jpeg,.png" onchange="validateFileType(this);">
            </div>
            <!-- <input class="img_encode" name="img_encode" style="display: none;" /> -->

            <button type="submit">Đăng ký</button>
        </form>
    </div>

</body>

</html>